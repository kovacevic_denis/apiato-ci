# Laravel Continuous Integration & Delivery with Bitbucket Pipelines and AWS Elastic Beanstalk

### Prerequisites
 - https://docs.docker.com/install/
 - https://docs.docker.com/compose/install/

### Installation
 1. ```git clone git@bitbucket.org:kovacevic_denis/apiato-ci.git```
 2. ```cd apiato-ci```
 3. ```bash Docker/setup.sh```
 4. Update your hosts file:
``` 127.0.0.1 apiato.test api.apiato.test ```

Done! You can access your app: 
> http://apiato.test
> http://api.apiato.test
> http://apiato.test/api/documentation
> http://apiato.test/api/private/documentation
